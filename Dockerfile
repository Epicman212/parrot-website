FROM node:current AS builder
COPY ./ /website
WORKDIR /website
RUN yarn install && yarn next build && yarn next export -o _build

FROM nginx:stable-alpine
COPY --from=builder /website/_build/ /usr/share/nginx/html/