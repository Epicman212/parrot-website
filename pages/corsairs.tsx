import { Grid, Typography } from '@mui/material'
import { makeStyles } from '@mui/styles'
import { NextPage } from 'next'

import JuniorCorsairs from 'containers/CorsairsContainers/CorsairSection/JuniorCorsairs'
import MasterCorsairs from 'containers/CorsairsContainers/CorsairSection/MasterCorsairs'
import SeniorCorsairs from 'containers/CorsairsContainers/CorsairSection/SeniorCorsairs'
import ProjectSection from 'containers/CorsairsContainers/ProjectSection'

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: 100
  },
  title: {
    marginTop: 10,
    paddingBottom: 20
  },
  headingSubTitle: {
    marginTop: 27,
    fontSize: 18,
    [theme.breakpoints.down('md')]: {
      fontSize: 16
    },
    marginBottom: theme.spacing(4)
  }
}))

const Corsairs: NextPage = () => {
  const classes = useStyles()

  return (
    <Grid container className={classes.root} justifyContent="center">
      <Grid
        container
        item
        xs={12}
        md={9}
        direction="column"
        className={classes.title}
        justifyContent="center"
      >
        <Typography variant="h1" align="center" paragraph>
          Parrot Corsairs
        </Typography>
        <Typography className={classes.headingSubTitle} variant="subtitle2Semi" align="center">
          At ParrotSec, we believe the strength of our project lies not just in the code itself but
          in the community that supports and improves it every day. The more you contribute, the
          more you are rewarded.
        </Typography>
      </Grid>
      <ProjectSection />
      <MasterCorsairs />
      <SeniorCorsairs />
      <JuniorCorsairs />
    </Grid>
  )
}

export default Corsairs
