import { Grid } from '@mui/material'
import { makeStyles } from '@mui/styles'
import { NextPage } from 'next'
import { SnackbarProvider } from 'notistack'

import StoreSection from 'containers/StoreContainers/StoreSection'

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: 100
  },
  snackbar: {
    background: 'linear-gradient(99.16deg, #05EEFF 24.01%, #00FFF0 81.75%)',
    color: theme.palette.text.primary,
    borderRadius: 16
  }
}))

const Store: NextPage = () => {
  const classes = useStyles()
  return (
    <SnackbarProvider preventDuplicate classes={{ variantSuccess: classes.snackbar }}>
      <Grid container className={classes.root} justifyContent="center">
        <StoreSection />
      </Grid>
    </SnackbarProvider>
  )
}

export default Store
