import { Grid, Typography } from '@mui/material'
import { makeStyles } from '@mui/styles'
import { NextPage } from 'next'
import { useRouter } from 'next/router'
import { SnackbarProvider } from 'notistack'

import 'react-image-lightbox/style.css'
import PStepper from 'components/PStepper'
import GetInvolvedSection from 'containers/ContributeContainers/GetInvolvedSection'
// import OSSelection from 'containers/DownloadContainers/OSSelection'

const useStyles = makeStyles(theme => ({
  snackbar: {
    backgroundColor: 'linear-gradient(99.16deg, #05EEFF 24.01%, #00FFF0 81.75%)',
    color: '#03232E',
    borderRadius: 16
  },
  root: {
    marginTop: 100
  },
  headingSubTitle: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(6.5)
  }
}))

const Download: NextPage = () => {
  const classes = useStyles()
  const router = useRouter()
  // const version =
  router.query.version === 'security' ||
  router.query.version === 'home' ||
  router.query.version === 'hackthebox' ||
  router.query.version === 'wsl' ||
  router.query.version === 'architect' ||
  router.query.version === 'cloud' ||
  router.query.version === 'raspberry'
    ? router.query.version
    : 'security'
  return (
    <SnackbarProvider preventDuplicate classes={{ variantSuccess: classes.snackbar }}>
      <Grid container justifyContent="center">
        <Typography className={classes.root} variant="h1" align="center" paragraph>
          Choose the right edition for you
        </Typography>
        <Typography className={classes.headingSubTitle} variant="subtitle2Semi" align="center">
          Follow the steps below to determine the edition of ParrotOS that best suits your needs and
          preferences.{' '}
        </Typography>
        <PStepper />
        {/*<OSSelection initialVersion={version} />*/}
        <GetInvolvedSection />
      </Grid>
    </SnackbarProvider>
  )
}

export default Download
