---
title: "Parrot 6.1 Release Notes"
date: 2024-06-05T12:39:23+02:00
author: palinuro
image: /assets/blog/6.1/parrot-6.1.png
description: "Parrot OS 6.1 is available for download."
---
[](/public/assets/blog/6.1/parrot-6.1.png)

We're excited to announce the release of Parrot 6.1, the new version of our operating system that includes in it numerous improvements and updates that makes the system more performing and stable.

#### How do I get ParrotOS?

You can download ParrotOS by clicking [here](https://www.parrotsec.org/download/). We strongly advise against using third-party download sources and recommend to stick to the official download links to ensure your security.

If you encounter any issue with direct downloads, we also provide Torrent files, which can help bypass firewalls and network restrictions in most cases.

#### How do I upgrade from a previous version?

You can upgrade an existing system via APT using one of the following commands:

*   `sudo parrot-upgrade`

or
*   `sudo apt update && sudo apt full-upgrade`

Before updating your version, we recommend backing up your data and performing a fresh installation of the latest version. This approach ensures a cleaner and more reliable user experience, particularly if you're upgrading from a significantly older version of Parrot.

## What's new in Parrot OS 6.1

#### Main system

This new release brings with it a lot of improvements and an update of several packages, as well as libraries, in particular the following ones:

*   **Anonsurf 4.2**: Improved stability and fixed issues in the launcher script for better anonymity and user experience.
*   **re-introduction of parrot-updater**: The update reminder popup was re-introduced to the system to assist keeping the system up to date.
*   **nmap**: Patched to fix several errors in its mssql scanning lua script.  
*   **burpsuite 2024.2.1.3**: Updated to the latest version and fixed a java version inconsistency on some machines.
*   **sqlmap 1.8.3**: Upgraded to the latest version for better SQL injection detection and testing capabilities.
*   **sslscan 2.1.3**: Updated to its latest version.
*   **zaproxy 2.14**: New version providing enhanced web application security testing tools.
*   **netexec 1.1.1**: Introduced to replace the now legacy crackmapexec tool. Netexec can be used as a drop-in replacement for crackmapexec and offers the same features.
*   **metasploit 6.4.6**: Updated with new exploits and improved penetration testing tools.
*   **woeusb-ng 0.2.12**: Upgraded to enhance the creation of bootable USB drives from Windows ISO files.
*   **volatility3 1.0.1**: Updated for better memory forensics and analysis capabilities.
*   **rizin 0.7.2**: New version providing enhanced reverse engineering tools and features.
*   **powershell-empire 5.9.5**: Updated to improve post-exploitation framework capabilities.
*   **instaloader 4.11**: Upgraded to the latest version for improved Instagram data scraping and downloading.
*   **gdb-gef 2024.1**: Updated with new features and improvements for the GDB Enhanced Features plugin.
*   **evil-winrm 3.5**: Enhanced version for better interaction with Windows Remote Management.
*   **bind9**: Applied an important security update.
*   **chromium**: Applied the latest security updates to ensure safe and secure browsing.
*   **firefox**: Updated with the latest security patches to enhance browsing security and performance.
*   **webkit**: Incorporated security updates to safeguard against vulnerabilities in the web rendering engine.
*   **golang 1.21**: Upgraded to the latest version, bringing performance improvements and new features.
*   **grub 2.12**: Updated to enhance bootloader functionality and security.
*   **libc6 and glibc6**: Security updates applied to improve system stability and security.
*   **pipewire audio server 1.0.5**: Updated for better audio handling and improved performance.
*   **libreoffice 24.2**: New version with enhanced features and security improvements for office suite applications.
*   **openjdk**: Security updates applied to improve Java runtime environment security.
*   **php8**: Incorporated security updates to enhance web development and server-side scripting security.
*   **ruby 3.1**: Updated with the latest security patches to improve development security and stability.
  
#### Raspberry Pi Images

*   **Raspberry Pi 5 support:** Added and enabled drivers to support the latest Raspberry Pi.
*   **Updated Kernel:** v6.6.28
*   **More compatibility:** Added new drivers to improve compatibility with external devices.
*   Fixed an issue that prevented WiFi from working properly on Raspberry Pi 400.

#### Debian Conversion Script

*   Improved code structure, making it more manageable and extensible for future updates.

#### Website

*   New "guided" download page. Choose the edition that best suits your needs. 
*   Small performance improvements.

#### Documentation

*   New mirrors added and updated.
*   Categories and documents have been reorganized for better UX.
*   Updated Docusaurus to the latest stable version.
*   Added new tutorials and updated all documents.

#### Ready-to-use Virtual Machines

Our ready-to-use virtual machines have been updated and are now available again. We offer these virtual machines for both amd64 and Apple Silicon (including M1 and M2), ensuring compatibility and convenience for a wide range of users.

This update guarantees that you can easily deploy the latest version of Parrot OS in your virtual environment, available for Security edition and Home edition.
