---
title: "Parrot Corsairs: join the crew"
date: 2024-05-02T10:28:18+02:00
author: sh4rk
image: /assets/blog/parrot-corsairs.png
description: "The more you contribute, the more you are rewarded."
---
[](/public/assets/blog/parrot-corsairs.png)

## Navigating Together: The Journey of ParrotSec Contributors
Have you ever thought about the impact one individual can have on an open-source project?

At ParrotSec, we believe the strength of our project lies not just in the code itself but in the community that supports and improves it every day.

That's why we're excited to introduce the ParrotSec Corsairs project, a new initiative designed to foster and recognize the contributions of our community members.

### How to Become a Corsair: Tiers and Rewards
Being a contributor has never been so easy. By clicking [here](https://gitlab.com/groups/parrotsec/-/milestones/1#tab-issues) you're going to see ParrotSec roadmap: choose the task that you want to work on and you're good to go!

You will have to conquer these 3 levels:  

*   **Junior Corsair**: Your journey begins with your first five successful commits on GitLab, earning you the title of Junior Corsair.  
    As a token of our appreciation, you'll receive the Parrot Swag Starter Pack, which includes a hoodie, T-shirt, and sticker sheet (take a look [here](https://hackthebox.store/collections/parrot-swags) to have some spoilers)  
    
*   **Senior Corsair**: The next step is maintaining active and successful contributions for six months, with at least one commit every two weeks.  
    Achieving this will grant you the Senior Corsair status, along with HTB Monthly VIP access as a reward, thanks to our collaboration with [Hack The Box](https://www.hackthebox.com/).  
    
*   **Master Corsair**: The pinnacle of achievement within our community, the Master Corsair tier is reserved for individuals handpicked by the Parrot team, based on exceptional contributions.  
    This prestigious recognition comes with HTB Monthly ProLabs access.  
    

### Celebrating Your Achievements
We believe in celebrating every milestone. That's why we're introducing a dedicated page on our website to honor the ParrotSec Corsairs: each contributor will be listed with a special badge corresponding to their tier.

Furthermore, we'll highlight the top three contributors across all categories on our social media platforms once per month, sharing your remarkable contributions with our broader community.

## Join Us on This Journey
Becoming a ParrotSec Corsair is more than just a title—it's a chance to be part of something bigger, to contribute to a project you love, and to be recognized for your efforts.
  
Take your first step today and join us in shaping the future of Parrot OS.  
Together, we can achieve incredible things.

Have thoughts, questions, or suggestions? We're all ears.
​​​​​​​The ParrotSec Corsairs project is community-driven at its core, and we value your input. Let's make Parrot OS even better, together.

## How can you get involved?
We have made available to the community a board dedicated to the issues and problems we generally encounter with the OS and its sub-projects. It can be viewed in our GitLab Milestone, and from there we keep track of everyone's contributions.  

### Register on GitLab
When registering with GitLab, it is important to specify your own Corsair name. This will be important for viewing and tracking your contributions.  

### Check the Milestone and start your Journey
Take a look at our main [Milestone](https://gitlab.com/groups/parrotsec/-/milestones/1#tab-issues) and identify the issue you are ready to contribute to, or open a new issue. Select it, and you will see the project to work on. Then start forking it; once you have worked on your fork, feel free to open a PR. The Parrot team will review the request and if it is approved, you will have just contributed to Parrot and earned your first step towards Junior Corsair status!


