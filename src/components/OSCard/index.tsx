import { Grid, GridProps, Paper, Typography, Theme } from '@mui/material'
import { createStyles, makeStyles } from '@mui/styles'
import cls from 'classnames'
import { ElementType } from 'react'

import PButton from 'components/PButton'

type OSCardProps = {
  Icon?: ElementType
  iconClassName?: string
  title?: string
  link?: string
  selected?: boolean
  onClick?: () => void
  isLinkCard?: boolean
  buttonText?: string
} & GridProps

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    versionsPaper: {
      padding: 32,
      height: '100%',
      display: 'flex',
      flexFlow: 'column',
      cursor: 'pointer',
      transition: 'box-shadow 0.3s ease-in-out',
      ...(theme.palette.mode === 'dark'
        ? {
            '&:hover': {
              boxShadow: '0 0 30px 10px #2c2981'
            }
          }
        : {})
    },
    iconHolder: {
      width: 64,
      height: 64,
      borderRadius: 6,
      display: 'flex',
      justifyContent: 'center',
      marginBottom: 21
    },
    icon: {
      width: 32,
      height: 32,
      margin: 'auto'
    },
    selectedCard: {
      borderColor: theme.palette.primary.main,
      borderWidth: 2,
      borderStyle: 'solid'
    }
  })
)

const OSCard = ({
  Icon,
  iconClassName,
  title,
  link,
  selected,
  onClick,
  isLinkCard,
  buttonText,
  children,
  ...rest
}: OSCardProps) => {
  const classes = useStyles()
  return (
    <Grid item xs={12} md={4} onClick={onClick} {...rest}>
      <Paper
        elevation={0}
        className={cls(classes.versionsPaper, { [classes.selectedCard]: selected })}
      >
        {Icon && (
          <div className={cls(classes.iconHolder, iconClassName)}>
            <Icon className={classes.icon} />
          </div>
        )}
        <Typography variant="h5" paragraph>
          {title}
        </Typography>
        <Typography variant="body1Semi" paragraph>
          {children}
        </Typography>
        {isLinkCard && link && buttonText && (
          <PButton
            variant="contained"
            color="primary"
            href={link}
            target="_blank"
            // style={{ marginTop: 16, maxWidth: 177 }}
          >
            {buttonText}
          </PButton>
        )}
      </Paper>
    </Grid>
  )
}

export default OSCard
