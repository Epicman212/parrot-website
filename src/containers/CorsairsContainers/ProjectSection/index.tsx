import { Grid, Typography, Paper } from '@mui/material'
import { makeStyles } from '@mui/styles'

import Milestone from './assets/milestone.png'

import PButton from 'components/PButton'
import PTimeline from 'components/Timeline'

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: 100
  },
  becomeCorsairTitle: {
    marginTop: theme.spacing(14),
    paddingBottom: theme.spacing(4)
  },
  content: {
    padding: theme.spacing(4),
    [theme.breakpoints.down('md')]: {
      paddingLeft: theme.spacing(4),
      paddingRight: theme.spacing(4)
    }
  },
  desc: {
    opacity: 0.5
  },
  responsiveJustify: {
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  }
}))

const ProjectSection = () => {
  const classes = useStyles()

  return (
    <>
      <Grid container item xs={12} md={9} spacing={4}>
        <Paper className={classes.root} elevation={0}>
          <Grid container item xs justifyContent="space-between">
            <Grid className={classes.content} container item xs={12} lg={4} direction="column">
              <Typography variant="h3" paragraph>
                Be part of the crew
              </Typography>
              <Typography className={classes.desc} variant="body1" paragraph>
                Ever wondered how much one person can really impact an open-source project? At
                ParrotSec, we know it&apos;s not just about the code; it&apos;s about the people who
                work on it day in, day out.
              </Typography>
              <Typography className={classes.desc} variant="body1" paragraph>
                Becoming a ParrotSec Corsair isn&apos;t just a fancy title. It&apos;s your chance to
                join a community, make a difference in something you care about, and get rewarded
                for your work.
              </Typography>
              <PButton
                variant="outlined"
                to="/blog/2024-05-02-parrot-corsairs-announcement/"
                style={{ maxWidth: 165 }}
              >
                Read more
              </PButton>
            </Grid>
            <Grid
              container
              item
              xs={12}
              lg
              direction="column"
              justifyContent="flex-end"
              alignItems="flex-end"
            >
              <img src={Milestone.src} alt="ParrotSec Milestone" />
            </Grid>
          </Grid>
        </Paper>
      </Grid>
      <Grid container item xs={12} md={9} spacing={4} justifyContent="center">
        <Typography variant="h2" align="center" paragraph className={classes.becomeCorsairTitle}>
          How to Become a Corsair: Tiers and Rewards
        </Typography>
        <PTimeline />
        <Paper elevation={0} sx={{ padding: 4 }}>
          <Typography variant="h4" align="center" paragraph sx={{ marginTop: 5 }}>
            How can you get involved?
          </Typography>
          <Typography
            className={classes.desc}
            sx={{ textAlign: 'center' }}
            variant="body1"
            paragraph
          >
            We have made available to the community a board dedicated to the issues and problems we
            generally encounter with the OS and its sub-projects. It can be viewed in our GitLab
            Milestone, and from there we keep track of everyone&apos;s contributions.
          </Typography>
          <Grid container item xs={12} spacing={2} sx={{ marginBottom: 4 }}>
            <Grid
              className={classes.responsiveJustify}
              container
              item
              xs={12}
              sm={6}
              justifyContent="flex-end"
            >
              <PButton
                variant="contained"
                to="https://gitlab.com/groups/parrotsec/-/milestones/1#tab-issues"
                gradient
              >
                GitLab Milestone
              </PButton>
            </Grid>
            <Grid className={classes.responsiveJustify} container item xs={12} sm={6}>
              <PButton variant="outlined" to="/blog/2024-05-02-parrot-corsairs-announcement/">
                Read more
              </PButton>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </>
  )
}

export default ProjectSection
